# Ferramenta de otimização para alocação de geração própria

Ferramenta computacional que possibilita a alocação ótima de geração própria de energia, utilizando o método Simplex.